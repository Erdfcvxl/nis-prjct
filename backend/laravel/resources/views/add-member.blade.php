@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add member to <b>{{ $group->name }}</b> group</div>

                    <div class="card-body">
                        <form id="form" method="POST" autocomplete="off" action="{{ route('add-new-member', $id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div id="suggestion" class="offset-4 col-md-6 position-relative">
                                    <div id="suggestion-container"></div>
                                    <div id="suggestion-loader" class="suggestion-loader" style="display: none">
                                        <div class="loader"></div>
                                        <span>{{ __("Looking for users") }}</span>
                                    </div>
                                    <div id="suggestion-not-found" class="suggestion-loader" style="display: none">
                                        <span style="margin: 0; width: 100%;">{{ __("User not found") }}</span>
                                        <button type="button" id="create-new-button" class="btn btn-sm btn-primary"
                                                style="min-width: 150px;">{{ __("Create new contact") }}</button>
                                    </div>
                                </div>

                            </div>


                            <div id="form-appendix" style="display: none">
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Full name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" disabled
                                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-mobile" type="tel" disabled
                                               class="form-control{{ $errors->has('contacts[mobile]') ? ' is-invalid' : '' }}"
                                               name="contacts[mobile]" value="{{ old('contacts[mobile]') }}">

                                        @if ($errors->has('contacts[mobile]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[mobile]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Work Mobile') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-work_mobile" type="tel" disabled
                                               class="form-control{{ $errors->has('contacts[work_mobile]') ? ' is-invalid' : '' }}"
                                               name="contacts[work_mobile]" value="{{ old('contacts[work_mobile]') }}">

                                        @if ($errors->has('contacts[work_mobile]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[work_mobile]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Work email') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-work_email" type="email" disabled
                                               class="form-control{{ $errors->has('contacts[work_email]') ? ' is-invalid' : '' }}"
                                               name="contacts[work_email]" value="{{ old('contacts[work_email]') }}">

                                        @if ($errors->has('contacts[work_email]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[work_email]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Contact language') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-contact_language" type="text" disabled
                                               class="form-control{{ $errors->has('contacts[contact_language]') ? ' is-invalid' : '' }}"
                                               name="contacts[contact_language]"
                                               value="{{ old('contacts[contact_language]') }}">

                                        @if ($errors->has('contacts[contact_language]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[contact_language]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                                    <div class="col-md-6">
                                        <select id="contacts-gender" type="text" disabled
                                                class="form-control{{ $errors->has('contacts[gender]') ? ' is-invalid' : '' }}"
                                                name="contacts[gender]" value="{{ old('contacts[gender]') }}">
                                            <option disabled selected value>-</option>
                                            <option value="0">Male</option>
                                            <option value="1">Female</option>
                                            <option value="2">Other</option>
                                        </select>

                                        @if ($errors->has('contacts[gender]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[gender]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-country" type="text" disabled
                                               class="form-control{{ $errors->has('contacts[country]') ? ' is-invalid' : '' }}"
                                               name="contacts[country]" value="{{ old('contacts[country]') }}">

                                        @if ($errors->has('contacts[country]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[country]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-city" type="text" disabled
                                               class="form-control{{ $errors->has('contacts[city]') ? ' is-invalid' : '' }}"
                                               name="contacts[city]" value="{{ old('contacts[city]') }}">

                                        @if ($errors->has('contacts[city]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[city]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-street" type="text" disabled
                                               class="form-control{{ $errors->has('contacts[street]') ? ' is-invalid' : '' }}"
                                               name="contacts[street]" value="{{ old('contacts[street]') }}">

                                        @if ($errors->has('contacts[street]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[street]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('House number') }}</label>

                                    <div class="col-md-6">
                                        <input id="contacts-house_number" type="number" disabled
                                               class="form-control{{ $errors->has('contacts[house_number]') ? ' is-invalid' : '' }}"
                                               name="contacts[house_number]"
                                               value="{{ old('contacts[house_number]') }}">

                                        @if ($errors->has('contacts[house_number]'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contacts[house_number]') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                            </div>

                            <button id="form-submit-button" class="disabled btn btn-success">{{ __('Add') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Prefab. Used for javascript --}}
    <div style="display: none;">
        <div id="suggestionPrefab">
            <ul class="suggestion">
                <li class="suggestion--item"></li>
            </ul>
        </div>
    </div>

    <script src="/js/autosuggestion.js"></script>
    <script>
        (function () {
            window['getUsersUrl'] = '{!! route('api.users-by-email') !!}';
            if (window.autosuggestion) {
                setTimeout(() => {
                    window.autosuggestion.init();
                }, 1000)
            }
        })();
    </script>
@endsection
