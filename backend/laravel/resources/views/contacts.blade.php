@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Your groups

                        <div class="float-right">
                            <a href="{{ route('new-group') }}" class="btn btn-sm btn-success">Create new Group</a>
                        </div>
                    </div>

                    <div class="card-body">
                        @if(! empty($groups))
                            <div class="row">
                                @foreach($groups as $group)
                                    <div class="col-md-4" style="margin-bottom: 24px;">
                                        <div class="card card-link-itself"
                                             onclick="window.location = '{{ route("contact-group", ["id" => $group->id]) }}'">
                                            <div class="card-body">
                                                <h5 class="card-title">{{ $group->name }}</h5>
                                                <p class="card-text">{{ $group->description }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        <hr>

                        <button href="{{ route('new-group') }}" class="btn btn-success">Create new group</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
