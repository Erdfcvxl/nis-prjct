@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ $group->name }}

                        <div class="float-right">
                            <a href="{{ route('new-member', $id) }}" class="btn btn-sm btn-success">{{ __("Add member") }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        {{ $group->description }}
                    </div>

                    <div class="card-body">
                        @if(! empty($groupUsers))
                            <div class="row">
                                @foreach($groupUsers as $groupUser)
                                    @if(\Auth::user()->id != $groupUser->user->id)
                                        <div class="col-md-6" style="margin-bottom: 24px;">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h5 class="card-title">{{ $groupUser->user->name }}{!! \Auth::user()->id == $groupUser->user->id ? " <small style='color: rgba(0,0,0,.5)'>(me)</small>" : '' !!}</h5>
                                                    <h6 class="card-subtitle mb-2 text-muted"><a
                                                                href="mailto:{{ $groupUser->user->email }}">{{ $groupUser->user->email }}</a>
                                                    </h6>
                                                    @if(! empty($groupUser->user->contacts))
                                                    <h6 class="card-subtitle mb-2 text-muted"><a
                                                                href="tel:{{ $groupUser->user->contacts->mobile }}">{{ $groupUser->user->contacts->mobile }}</a>
                                                    </h6>

                                                    <p class="card-text">
                                                        {{ composeAddress($groupUser->user->contacts) }}
                                                    </p>
                                                    @endif

                                                    {{--<a href=""  class="btn btn-sm btn-outline-info">Share</a>--}}
                                                    <a href="{{ route('user-settings', $groupUser->user->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>
                                                    <a href="{{ route('remove-member', [$id, $groupUser->user->id]) }}" {{ \Auth::user()->id == $group->owner ? 'disabled' : '' }} class="btn btn-sm btn-outline-danger">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
