@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Settings</div>

                    <div class="card-body">
                        <h4>Account information</h4>
                        <form method="POST" action="{{ isset($id) ? route('update-settings', $id) : route('update-my-settings') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') ? old('email') : $user->email }}"
                                           required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Full name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') ? old('name') : $user->name }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <button class="btn btn-success" type="submit">{{ __('Save') }}</button>
                        </form>

                        <hr>

                        <h4>Contact info</h4>
                        <form method="POST" action="{{ isset($id) ? route('update-contacts', $id) : route('update-my-contacts') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="mobile"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Mobile number') }}</label>

                                <div class="col-md-6">
                                    <input id="mobile" type="tel"
                                           class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}"
                                           name="mobile" value="{{ old('mobile') ? old('mobile') : $contacts->mobile }}"
                                           required autofocus>

                                    @if ($errors->has('mobile'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="country"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                                <div class="col-md-6">
                                    <input id="country" type="tel"
                                           class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}"
                                           name="country"
                                           value="{{ old('country') ? old('country') : $contacts->country }}" required
                                           autofocus>

                                    @if ($errors->has('country'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city"
                                       class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                                <div class="col-md-6">
                                    <input id="city" type="tel"
                                           class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}"
                                           name="city" value="{{ old('city') ? old('city') : $contacts->city }}"
                                           required autofocus>

                                    @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="street"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>

                                <div class="col-md-6">
                                    <input id="street" type="tel"
                                           class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}"
                                           name="street" value="{{ old('street') ? old('street') : $contacts->street }}"
                                           required autofocus>

                                    @if ($errors->has('street'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="house_number"
                                       class="col-md-4 col-form-label text-md-right">{{ __('House number') }}</label>

                                <div class="col-md-6">
                                    <input id="house_number" type="tel"
                                           class="form-control{{ $errors->has('house_number') ? ' is-invalid' : '' }}"
                                           name="house_number"
                                           value="{{ old('house_number') ? old('house_number') : $contacts->house_number }}"
                                           required autofocus>

                                    @if ($errors->has('house_number'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('house_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <button class="btn btn-success" type="submit">{{ __('Save') }}</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
