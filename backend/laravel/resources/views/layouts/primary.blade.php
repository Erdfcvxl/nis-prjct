<html>
<head>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap-grid.css">
    <link rel="stylesheet" href="/css/bootstrap-reboot.css">

    <scrip src="/js/app.js"></scrip>
    <scrip src="/js/bootstrap.bundle.js"></scrip>
</head>
<body>

    @yield('main')

</body>
</html>