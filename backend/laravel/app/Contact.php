<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = "contacts";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'mobile',
        'work_mobile',
        'work_email',
        'contact_language',
        'gender',
        'country',
        'city',
        'street',
        'house_number',
    ];
}
