<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = "groups";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner',
        'name',
        'description',
    ];

    public function GroupUsers()
    {
        return $this->hasMany(GroupUser::class, 'group_id', 'id');
    }
}
