<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function getUsersByEmail(Request $request)
    {
        return User::where('email', 'like', '%'. $request->get('email') .'%')->orderBy('email')->limit(5)->get();
    }
}
