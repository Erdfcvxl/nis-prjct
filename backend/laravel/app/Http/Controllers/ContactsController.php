<?php

namespace App\Http\Controllers;

use App\Group;
use App\GroupUser;
use App\Services\ContactService;
use App\Services\GroupService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use PhpParser\Error;

class ContactsController extends Controller
{
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->middleware('auth');

        $this->contactService = $contactService;
    }

    public function index()
    {
        return view('contacts', [
            'groups' => $myGroups = $this->contactService->getMyGroups()
        ]);
    }

    public function viewGroup($id)
    {
        $group = $this->contactService->getGroup($id);

        if (empty($group)) {
            abort(404);
        }

        return view('contact-group', [
            'id'         => $id,
            'group'      => $group,
            'groupUsers' => $group->groupUsers
        ]);
    }

    public function newGroup()
    {
        $group = new Group();

        return view('new-group', [
            'model' => $group
        ]);
    }

    public function createNewGroup(Request $request)
    {
        $group = new Group($request->all());
        /** @noinspection PhpUndefinedClassInspection */
        $group->owner = \Auth::User()->id;
        $group->save();

        return redirect(route('contact-group', $group->id));
    }

    public function newMember($id)
    {
        $userService = resolve(UserService::class);
        $group = $this->contactService->getGroup($id);


        if (!$group) {
            return redirect()->back();
        }

        return view('add-member', [
            'id'       => $id,
            'group'    => $group,
            'allUsers' => $userService->getAll()
        ]);
    }

    public function addNewMember(Request $request, $id)
    {
        $validatedData = $request->validate([
            'email' => 'required|email'
        ]);

        $userService = resolve(UserService::class);
        $groupService = resolve(GroupService::class);

        $contacts = (!empty($request->only('contacts'))) ? $request->only('contacts')['contacts'] : null;

        if ($user = $userService->getOrCreate($request->except('contacts'), $contacts)) {
            // Add user to the group
            $groupService->addUserToGroup($user->id, $id);

            return redirect(route('contact-group', $id));
        }
    }

    public function removeMember($groupId, $userId)
    {
        $groupUsers = GroupUser::where(['group_id' => $groupId, 'user_id' => $userId])->get();

        foreach ($groupUsers as $groupUser) {
            $groupUser->delete();
        }

        return redirect()->back()->with([
            'flash' => [
                [
                    'type'    => 'success',
                    'heading' => 'Done',
                    'message' => 'Removed successfully'
                ]
            ]
        ]);
    }
}
