<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth/login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // Check if user was created by other user
        if ($user = User::where(['email' => $request->get('email')])->first()) {
            if ($user->password === "-1") {
                return redirect()->route('add-password', $user->id);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function addPassword($id)
    {
        if ($user = User::where(['id' => $id])->first()) {
            if ($user->password !== "-1") {
                abort(404);
            }
        } else {
            abort(404);
        }

        return view('auth/add-password', [
            'id' => $id
        ]);
    }

    public function saveAddedPassword(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        if ($user = User::where(['id' => $id])->first()) {
            if ($user->password === "-1") {
                $user->password = bcrypt($request->get('password'));
                $user->save();

                \Auth::login($user, true);
            }

            return redirect()->route('home')->with([
                'flash' => [
                    [
                        'type'    => 'success',
                        'heading' => 'Done',
                        'message' => 'Registration finished'
                    ]
                ]
            ]);
        }

        return redirect()->back()->with([
            'flash' => [
                [
                    'type'    => 'error',
                    'heading' => 'Whoops',
                    'message' => 'Something went wrong'
                ]
            ]
        ]);

    }
}
