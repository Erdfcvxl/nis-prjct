<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function mySettings()
    {
        $user = \Auth::user();
        $contacts = $user->contacts;
        return view('settings', [
            'user'     => $user,
            'contacts' => $contacts
        ]);
    }

    public function updateMySettings(Request $request)
    {
        if (\Auth::user()->update($request->all())) {
            return redirect()->back()->with([
                'flash' => [
                    [
                        'type'    => 'success',
                        'heading' => 'success',
                        'message' => 'Account settings were updated'
                    ]
                ]
            ]);
        } else {
            return redirect()->back()->with([
                'flash' => [
                    [
                        'type'    => 'warning',
                        'heading' => 'Error',
                        'message' => 'Account settings were not updated'
                    ]
                ]
            ]);
        }
    }

    public function updateMyContacts(Request $request)
    {
        if (\Auth::user()->contacts->update($request->all())) {
            return redirect()->back()->with([
                'flash' => [
                    [
                        'type'    => 'success',
                        'heading' => 'success',
                        'message' => 'Account settings were updated'
                    ]
                ]
            ]);
        } else {
            return redirect()->back()->with([
                'flash' => [
                    [
                        'type'    => 'warning',
                        'heading' => 'Error',
                        'message' => 'Account settings were not updated'
                    ]
                ]
            ]);
        }
    }

    public function userSettings($id)
    {
        $user = User::where(['id' => $id])->first();

        return view('settings', [
            'id' => $id,
            'user' => $user,
            'contacts' => $user->contacts
        ]);
    }

    public function updateSettings(Request $request, $id)
    {
        $user = User::where(['id' => $id])->first();
        $user->update($request->all());

        return redirect()->back()->with([
            'flash' => [
                [
                    'type'    => 'success',
                    'heading' => 'Done',
                    'message' => 'Settings updated successfully'
                ]
            ]
        ]);
    }

    public function updateContacts(Request $request, $id)
    {
        $user = User::where(['id' => $id])->first();
        $user->contacts->update($request->all());

        return redirect()->back()->with([
            'flash' => [
                [
                    'type'    => 'success',
                    'heading' => 'Done',
                    'message' => 'Contacts updated successfully'
                ]
            ]
        ]);
    }
}
