<?php
/**
 * Created by PhpStorm.
 * User: Erdfcvxl
 * Date: 1/15/2019
 * Time: 3:21 PM
 */

namespace App\Services;


use App\Group;

class ContactService
{
    protected $currentUser;

    public function __construct()
    {
        $this->currentUser = \Auth::user();
    }

    private function getCurrentUser()
    {
        if (!empty($this->currentUser)) {
            return $this->currentUser;
        }

        return $this->currentUser = \Auth::user();;
    }

    public function getMyGroups()
    {
        if ($this->getCurrentUser()) {
            if (!empty($groups = $this->currentUser->groups)) {
                $result = [];

                foreach ($groups as $group) {
                    $result[] = $group->group;
                }

                return $result;
            }
        }

        return -1;
    }

    public function getGroup($id)
    {
        return Group::where(['id' => $id])->first();
    }
}