<?php
/**
 * Created by PhpStorm.
 * User: Erdfcvxl
 * Date: 1/15/2019
 * Time: 3:21 PM
 */

namespace App\Services;


use App\Contact;
use App\User;

class UserService
{
    protected $currentUser;

    public function __construct(User $user)
    {

        //$currentUser =
    }

    public function getAll()
    {
        return User::get();
    }

    public function getUserByEmail($email)
    {
        return User::where(['email' => $email])->first();
    }

    public function getOrCreate($attributes, $contacts)
    {
        if(!($user = $this->getUserByEmail($attributes['email']))) {
            $user = new User($attributes);
            $user->save();

            // Add contacts
            $contacts = new Contact($contacts);
            $contacts->user_id = $user->id;

            $contacts->save();
        }

        return $user;
    }

    public function createUser($attributes)
    {
        $user = new User($attributes);
        return $user->save();
    }


}