<?php
/**
 * Created by PhpStorm.
 * User: Erdfcvxl
 * Date: 1/15/2019
 * Time: 3:21 PM
 */

namespace App\Services;


use App\Contact;
use App\GroupUser;
use App\User;

class GroupService
{
    public function addUserToGroup($userId, $groupId)
    {
        // If this user does not exists already
        if (!GroupUser::where([
                    'user_id'  => $userId,
                    'group_id' => $groupId
                ])->first()) {

            // Add user to the group
            $groupUser = new GroupUser([
                'user_id'  => $userId,
                'group_id' => $groupId
            ]);

            $groupUser->save();
        }
    }
}