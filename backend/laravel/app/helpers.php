<?php

if (!function_exists('composeAddress')) {
    function composeAddress($contacts)
    {
        $result =
            ($contacts->street ? ($contacts->house_number ? $contacts->street . " " . $contacts->house_number . ", " : $contacts->street . ", ") : '') .
            $contacts->city . ($contacts->city ? ", " : "") .
            $contacts->country;

        return $result;
    }
}