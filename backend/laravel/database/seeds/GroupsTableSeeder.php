<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table("groups")->insert([
            "owner"       => 10,
            "name"        => "Developers",
            "description" => "Developers which worked on contact ContactManager project"
        ]);

        //2
        DB::table("groups")->insert([
            "owner"       => 10,
            "name"        => "Freelance developers",
            "description" => "People who can be contacted to develop a product"
        ]);

        //3
        DB::table("groups")->insert([
            "owner"       => 10,
            "name"        => "Freelance designers",
            "description" => "People who can be contacted to design a product"
        ]);

        //4
        DB::table("groups")->insert([
            "owner"       => 10,
            "name"        => "Project managers",
            "description" => "Project managers which managed ContactManager project work process"
        ]);


        // Rokas Rudgalvis
        DB::table("group_users")->insert(["group_id" => 1, "user_id" => 1]);
        DB::table("group_users")->insert(["group_id" => 2, "user_id" => 1]);
        DB::table("group_users")->insert(["group_id" => 3, "user_id" => 1]);

        // Rokas Matuzonis
        DB::table("group_users")->insert(["group_id" => 1, "user_id" => 2]);
        DB::table("group_users")->insert(["group_id" => 3, "user_id" => 2]);

        // Efren Silva
        DB::table("group_users")->insert(["group_id" => 1, "user_id" => 3]);
        DB::table("group_users")->insert(["group_id" => 3, "user_id" => 3]);
        DB::table("group_users")->insert(["group_id" => 4, "user_id" => 3]);

        // Others
        DB::table("group_users")->insert(["group_id" => 2, "user_id" => 4]);

        DB::table("group_users")->insert(["group_id" => 2, "user_id" => 5]);
        DB::table("group_users")->insert(["group_id" => 3, "user_id" => 5]);

        DB::table("group_users")->insert(["group_id" => 3, "user_id" => 6]);

        DB::table("group_users")->insert(["group_id" => 3, "user_id" => 7]);

        DB::table("group_users")->insert(["group_id" => 2, "user_id" => 9]);

        DB::table("group_users")->insert(["group_id" => 1, "user_id" => 10]);
        DB::table("group_users")->insert(["group_id" => 2, "user_id" => 10]);
        DB::table("group_users")->insert(["group_id" => 3, "user_id" => 10]);
        DB::table("group_users")->insert(["group_id" => 4, "user_id" => 10]);

    }
}
