<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('users')->insert([
            "name" => "Rokas Rudgalvis",
            "email" => "rokasr788@gmail.com",
            "password" => bcrypt("erasmus")
        ]);

        //2
        DB::table('users')->insert([
            "name" => "Rokas Matuzonis",
            "email" => "matuzonis@gmail.com",
            "password" => bcrypt("erasmus")
        ]);

        //3
        DB::table('users')->insert([
            "name" => "Efren Silva",
            "email" => "silva@gmail.com",
            "password" => bcrypt("erasmus")
        ]);

        //4
        DB::table('users')->insert([
            "name" => "Tomas Martinkus",
            "email" => "tomas.martinkus@email.com",
            "password" => bcrypt("erasmus")
        ]);

        //5
        DB::table('users')->insert([
            "name" => "Matas Dziadaravičius",
            "email" => "m.dziadas@email.com",
            "password" => bcrypt("erasmus")
        ]);

        //6
        DB::table('users')->insert([
            "name" => "Laurynas Kapačinskas",
            "email" => "l.kapacinskas@email.com",
            "password" => bcrypt("erasmus")
        ]);


        //7
        DB::table('users')->insert([
            "name" => "Vilma Razmutė",
            "email" => "v.razmute@email.com",
            "password" => bcrypt("erasmus")
        ]);

        //8
        DB::table('users')->insert([
            "name" => "Brigita Šermukšnytė",
            "email" => "b.sermuksnyte@email.com",
            "password" => bcrypt("erasmus")
        ]);

        //9
        DB::table('users')->insert([
            "name" => "Deividas Karūnos",
            "email" => "deivis827@email.com",
            "password" => bcrypt("erasmus")
        ]);

        //10
        DB::table('users')->insert([
            "name" => "Mr. View Viewer",
            "email" => "demo@demo.com",
            "password" => bcrypt("demo")
        ]);
    }
}
