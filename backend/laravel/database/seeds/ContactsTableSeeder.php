<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            "user_id" => 1,
            "mobile" => "+370 657 00000",
            "work_mobile" => "+370 657 11111",
            "work_email" => "rr@work.mail",
            "contact_language" => "English",
            "gender" => "male",
            "country" => "Lithuania",
            "city" => "Vilnius",
            "street" => "Balstogės gatvė",
            "house_number" => "8",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 2,
            "mobile" => "+370 123 54321",
            "work_mobile" => "+370 543 21234",
            "work_email" => "rmatuz@work.mail",
            "contact_language" => "English",
            "gender" => "male",
            "country" => "Lithuania",
            "city" => "Vilnius",
            "street" => "Balsių gatvė",
            "house_number" => "1",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 3,
            "mobile" => "+370 355 89012",
            "work_mobile" => "+370 978 79803",
            "work_email" => "es@work.mail",
            "contact_language" => "Spanish",
            "gender" => "male",
            "country" => "Spain",
            "city" => "Barcelona",
            "street" => "Carrer de les Ànimes",
            "house_number" => "16",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 4,
            "mobile" => "+370 657 00000",
            "work_mobile" => "+370 657 11111",
            "work_email" => "tm@work.mail",
            "contact_language" => "English",
            "gender" => "male",
            "country" => "Lithuania",
            "city" => "Vilnius",
            "street" => "Putino",
            "house_number" => "3",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 5,
            "mobile" => "+370 657 00000",
            "work_mobile" => "+370 657 11111",
            "work_email" => "md@work.mail",
            "contact_language" => "English",
            "gender" => "male",
            "country" => "Lithuania",
            "city" => "Vilnius",
            "street" => "Balstogės gatvė",
            "house_number" => "8",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 6,
            "mobile" => "+370 657 00000",
            "work_mobile" => "+370 657 11111",
            "work_email" => "lk@work.mail",
            "contact_language" => "English",
            "gender" => "male",
            "country" => "Lithuania",
            "city" => "Vilnius",
            "street" => "Milasiaus gatvė",
            "house_number" => "12",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 7,
            "mobile" => "+370 657 00000",
            "work_mobile" => "+370 657 1111",
            "work_email" => "vr@work.mail",
            "contact_language" => "English",
            "gender" => "female",
            "country" => "Lithuania",
            "city" => "Vilnius",
            "street" => "Pilies gatvė",
            "house_number" => "12",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 8,
            "mobile" => "+370 657 00000",
            "work_mobile" => "+370 657 11111",
            "work_email" => "bs@work.mail",
            "contact_language" => "English",
            "gender" => "female",
            "country" => "Lithuania",
            "city" => "Vilnius",
            "street" => "Antakalnio gatvė",
            "house_number" => "87",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 9,
            "mobile" => "+370 657 00000",
            "work_mobile" => "+370 657 11111",
            "work_email" => "dk@work.mail",
            "contact_language" => "English",
            "gender" => "male",
            "country" => "Scotland",
            "city" => "Edinburgh",
            "street" => "Wardlaw street",
            "house_number" => "18",
        ]);
        DB::table('contacts')->insert([
            "user_id" => 10,
            "mobile" => "+000 000 00000",
            "work_mobile" => "+888 888 88888",
            "work_email" => "ww@work.mail",
            "contact_language" => "English",
            "gender" => "male",
            "country" => "Portugal",
            "city" => "Viana do Castelo",
            "street" => "Humberto Delgado",
            "house_number" => "101",
        ]);
    }
}
