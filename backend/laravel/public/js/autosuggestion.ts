class Autosuggestion {
    private _container: HTMLElement;
    private _emailInput: HTMLInputElement;
    private _form: HTMLElement;
    private _formAppendix: HTMLElement;
    private _loader: HTMLElement;
    private _newUserButton: HTMLInputElement;
    private _notFound: HTMLElement;
    private _prefab: HTMLElement;
    private _submitButton: HTMLElement;

    private _xhr: XMLHttpRequest;

    /**
     * Initialize autosuggestion
     **/
    init() {

        console.log('init');
        this._container = <HTMLElement>document.querySelector('#suggestion-container');
        this._emailInput = <HTMLInputElement>document.querySelector('#email');
        this._form = <HTMLElement>document.querySelector('#form');
        this._formAppendix = <HTMLElement>document.querySelector('#form-appendix');
        this._loader = <HTMLElement>document.querySelector('#suggestion-loader');
        this._newUserButton = <HTMLInputElement>document.querySelector('#create-new-button');
        this._notFound = <HTMLElement>document.querySelector('#suggestion-not-found');
        this._prefab = <HTMLElement>document.querySelector('#suggestionPrefab').firstElementChild;
        this._submitButton = <HTMLElement>document.querySelector('#form-submit-button');

        if (!this._container ||
            !this._emailInput ||
            !this._form ||
            !this._formAppendix ||
            !this._loader ||
            !this._newUserButton ||
            !this._notFound ||
            !this._prefab ||
            !this._submitButton) {
            console.log('Error trying initialize autosuggestion');
            return;
        }

        this.addEvents();
        this.disableSubmit();
    }

    /**
     *  Start listening input change (input) event
     **/
    private addEvents() {
        this._emailInput.addEventListener('input', (e) => {
            const value = this._emailInput.value;
            this.findUsers(value);
        });

        this._newUserButton.addEventListener('click', () => {
            this.showFormAppendix();
        });
    }


    // Ajax call
    /**
     * Creates XML Http Request (AJAX)
     **/
    private findUsers(email) {
        if (email.length > 2) {
            if (this._xhr) {
                this._xhr.abort();
            }

            this.showLoader();

            this._xhr = new XMLHttpRequest();
            this._xhr.open('POST', window['getUsersUrl']);
            this._xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            this._xhr.setRequestHeader('Content-Type', 'application/json');
            this._xhr.responseType = 'json';

            this._xhr.onreadystatechange = () => {
                if (this._xhr.readyState === 4) { // readyState 4 means the request is done.
                    if (this._xhr.status === 200) { // status 200 is a successful return.
                        this.handleResponse(this._xhr.response);
                        this.hideLoader();
                    }
                } else {
                    console.log('Error: ' + this._xhr.status); // An error occurred during the request.
                    this.hideLoader();
                }
            };

            this._xhr.send(JSON.stringify({email}));
        } else {
            this.hideLoader();
            this.hideNotFound();
            this.hideSuggestions();
        }
    }

    /**
     * Handles XML Http Request's response
     **/
    private handleResponse(response: any[]) {
        this.clearContainer();

        if (response.length > 0) {
            const suggestionUl = <HTMLElement>this._prefab.cloneNode();

            response.forEach(user => {
                const li = <HTMLElement>this._prefab.firstElementChild.cloneNode();
                li.innerHTML = user.email;
                // li.setAttribute('data-user-object', JSON.stringify(user));
                li.addEventListener('click', () => {
                    return this.handleSuggestionSelect(user);
                });

                suggestionUl.appendChild(li);
            });

            this.showSuggestions();
            this._container.appendChild(suggestionUl);
            suggestionUl.style.display = 'block';
        } else {
            this.showNotFound();
        }
    }

    private handleSuggestionSelect(user: any) {
        this._emailInput.value = user.email;
        this.hideSuggestions();
        this.enableSubmit();
    }


    // Helpers
    /**
     * Removes every DOM node from container element
     **/
    private clearContainer() {
        while (this._container.hasChildNodes()) {
            this._container.removeChild(this._container.firstChild);
        }
    }

    private showLoader() {
        this.hideSuggestions();
        this._loader.setAttribute('style', '');
        this.hideNotFound();
    }

    private hideLoader() {
        this._loader.setAttribute('style', 'display: none');
    }

    private showSuggestions() {
        this._container.setAttribute('style', '');
        this.hideLoader();
        this.hideNotFound();
    }

    private hideSuggestions() {
        this._container.setAttribute('style', 'display: none');
    }

    private showNotFound() {
        this.hideSuggestions();
        this.hideLoader();
        this._notFound.setAttribute('style', '');
    }

    private hideNotFound() {
        this._notFound.setAttribute('style', 'display: none');
    }

    private static preventDefault(e) {
        e.preventDefault();
    }

    private enableSubmit() {
        this._form.removeEventListener('submit', Autosuggestion.preventDefault);
        this._submitButton.classList.remove('disabled');
    }

    private disableSubmit() {
        this._form.addEventListener('submit', Autosuggestion.preventDefault);
        this._submitButton.classList.add('disabled');
    }

    private hideFormAppendix() {
        this._formAppendix.style.display = 'none';

        // Disable every input
        const inputs = this._formAppendix.querySelectorAll('input, select');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].setAttribute('disabled', 'true');
        }
    }

    private showFormAppendix() {
        this.hideNotFound();
        this.enableSubmit();
        this._formAppendix.style.display = 'block';

        // Enable every input
        const inputs = this._formAppendix.querySelectorAll('input, select');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].removeAttribute('disabled');
        }
    }
}

// Add to global window variable
(function () {
    window['autosuggestion'] = new Autosuggestion();
})();