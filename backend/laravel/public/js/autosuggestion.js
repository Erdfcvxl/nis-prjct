var Autosuggestion = /** @class */ (function () {
    function Autosuggestion() {
    }
    /**
     * Initialize autosuggestion
     **/
    Autosuggestion.prototype.init = function () {
        console.log('init');
        this._container = document.querySelector('#suggestion-container');
        this._emailInput = document.querySelector('#email');
        this._form = document.querySelector('#form');
        this._formAppendix = document.querySelector('#form-appendix');
        this._loader = document.querySelector('#suggestion-loader');
        this._newUserButton = document.querySelector('#create-new-button');
        this._notFound = document.querySelector('#suggestion-not-found');
        this._prefab = document.querySelector('#suggestionPrefab').firstElementChild;
        this._submitButton = document.querySelector('#form-submit-button');
        if (!this._container ||
            !this._emailInput ||
            !this._form ||
            !this._formAppendix ||
            !this._loader ||
            !this._newUserButton ||
            !this._notFound ||
            !this._prefab ||
            !this._submitButton) {
            console.log('Error trying initialize autosuggestion');
            return;
        }
        this.addEvents();
        this.disableSubmit();
    };
    /**
     *  Start listening input change (input) event
     **/
    Autosuggestion.prototype.addEvents = function () {
        var _this = this;
        this._emailInput.addEventListener('input', function (e) {
            var value = _this._emailInput.value;
            _this.findUsers(value);
        });
        this._newUserButton.addEventListener('click', function () {
            _this.showFormAppendix();
        });
    };
    // Ajax call
    /**
     * Creates XML Http Request (AJAX)
     **/
    Autosuggestion.prototype.findUsers = function (email) {
        var _this = this;
        if (email.length > 2) {
            if (this._xhr) {
                this._xhr.abort();
            }
            this.showLoader();
            this._xhr = new XMLHttpRequest();
            this._xhr.open('POST', window['getUsersUrl']);
            this._xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            this._xhr.setRequestHeader('Content-Type', 'application/json');
            this._xhr.responseType = 'json';
            this._xhr.onreadystatechange = function () {
                if (_this._xhr.readyState === 4) { // readyState 4 means the request is done.
                    if (_this._xhr.status === 200) { // status 200 is a successful return.
                        _this.handleResponse(_this._xhr.response);
                        _this.hideLoader();
                    }
                }
                else {
                    console.log('Error: ' + _this._xhr.status); // An error occurred during the request.
                    _this.hideLoader();
                }
            };
            this._xhr.send(JSON.stringify({ email: email }));
        }
        else {
            this.hideLoader();
            this.hideNotFound();
            this.hideSuggestions();
        }
    };
    /**
     * Handles XML Http Request's response
     **/
    Autosuggestion.prototype.handleResponse = function (response) {
        var _this = this;
        this.clearContainer();
        if (response.length > 0) {
            var suggestionUl_1 = this._prefab.cloneNode();
            response.forEach(function (user) {
                var li = _this._prefab.firstElementChild.cloneNode();
                li.innerHTML = user.email;
                // li.setAttribute('data-user-object', JSON.stringify(user));
                li.addEventListener('click', function () {
                    return _this.handleSuggestionSelect(user);
                });
                suggestionUl_1.appendChild(li);
            });
            this.showSuggestions();
            this._container.appendChild(suggestionUl_1);
            suggestionUl_1.style.display = 'block';
        }
        else {
            this.showNotFound();
        }
    };
    Autosuggestion.prototype.handleSuggestionSelect = function (user) {
        this._emailInput.value = user.email;
        this.hideSuggestions();
        this.enableSubmit();
    };
    // Helpers
    /**
     * Removes every DOM node from container element
     **/
    Autosuggestion.prototype.clearContainer = function () {
        while (this._container.hasChildNodes()) {
            this._container.removeChild(this._container.firstChild);
        }
    };
    Autosuggestion.prototype.showLoader = function () {
        this.hideSuggestions();
        this._loader.setAttribute('style', '');
        this.hideNotFound();
    };
    Autosuggestion.prototype.hideLoader = function () {
        this._loader.setAttribute('style', 'display: none');
    };
    Autosuggestion.prototype.showSuggestions = function () {
        this._container.setAttribute('style', '');
        this.hideLoader();
        this.hideNotFound();
    };
    Autosuggestion.prototype.hideSuggestions = function () {
        this._container.setAttribute('style', 'display: none');
    };
    Autosuggestion.prototype.showNotFound = function () {
        this.hideSuggestions();
        this.hideLoader();
        this._notFound.setAttribute('style', '');
    };
    Autosuggestion.prototype.hideNotFound = function () {
        this._notFound.setAttribute('style', 'display: none');
    };
    Autosuggestion.preventDefault = function (e) {
        e.preventDefault();
    };
    Autosuggestion.prototype.enableSubmit = function () {
        this._form.removeEventListener('submit', Autosuggestion.preventDefault);
        this._submitButton.classList.remove('disabled');
    };
    Autosuggestion.prototype.disableSubmit = function () {
        this._form.addEventListener('submit', Autosuggestion.preventDefault);
        this._submitButton.classList.add('disabled');
    };
    Autosuggestion.prototype.hideFormAppendix = function () {
        this._formAppendix.style.display = 'none';
        // Disable every input
        var inputs = this._formAppendix.querySelectorAll('input, select');
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].setAttribute('disabled', 'true');
        }
    };
    Autosuggestion.prototype.showFormAppendix = function () {
        this.hideNotFound();
        this.enableSubmit();
        this._formAppendix.style.display = 'block';
        // Enable every input
        var inputs = this._formAppendix.querySelectorAll('input, select');
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].removeAttribute('disabled');
        }
    };
    return Autosuggestion;
}());
// Add to global window variable
(function () {
    window['autosuggestion'] = new Autosuggestion();
})();
