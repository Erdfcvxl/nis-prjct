<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['api'])->name('api.')->group(function () {
    Route::get('/users', ['as' => 'users-by-email', 'uses' => 'APIController@getUsersByEmail']);
    Route::post('/users', ['as' => 'users-by-email', 'uses' => 'APIController@getUsersByEmail']);
});