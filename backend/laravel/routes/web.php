<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Landing page */
Route::get('/', ['as' => '', 'uses' => 'Auth\LoginController@index']);
Route::get('/add-psw/{id}', ['as' => 'add-password', 'uses' => 'Auth\LoginController@addPassword']);
Route::post('/add-psw/{id}', ['as' => 'save-added-password', 'uses' => 'Auth\LoginController@saveAddedPassword']);

/* Settings */
Route::get('/mySettings', ['as' => 'my-settings', 'uses' => 'SettingsController@mySettings']);
Route::post('/update-my-settings', ['as' => 'update-my-settings', 'uses' => 'SettingsController@updateMySettings']);
Route::post('/update-my-contacts', ['as' => 'update-my-contacts', 'uses' => 'SettingsController@updateMyContacts']);
Route::get('/settings/{id}', ['as' => 'user-settings', 'uses' => 'SettingsController@userSettings']);
Route::post('/update-settings/{id}', ['as' => 'update-settings', 'uses' => 'SettingsController@updateSettings']);
Route::post('/update-contacts/{id}', ['as' => 'update-contacts', 'uses' => 'SettingsController@updateContacts']);

/* Contacts */
Route::get('/contacts', ['as' => 'contacts', 'uses' => 'ContactsController@index']);
Route::get('/contact-group/{id}', ['as' => 'contact-group', 'uses' => 'ContactsController@viewGroup']);
Route::get('/new-group', ['as' => 'new-group', 'uses' => 'ContactsController@newGroup']);
Route::post('/new-group', ['as' => 'create-new-group', 'uses' => 'ContactsController@createNewGroup']);
Route::get('/add-member/{id}', ['as' => 'new-member', 'uses' => 'ContactsController@newMember']);
Route::post('/add-member/{id}', ['as' => 'add-new-member', 'uses' => 'ContactsController@addNewMember']);
Route::get('/remove-member/{id}/{memberId}', ['as' => 'remove-member', 'uses' => 'ContactsController@removeMember']);

/* Authentication */
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
