#!/bin/bash

# -p        Project name
# --build   Build before running containers
# --detach  Run in background
docker-compose -p nsi-si-prjct-dev -f docker-compose-dev.yml up --build -d