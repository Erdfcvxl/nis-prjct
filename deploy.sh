#!/bin/bash

# -p        Project name
# --build   Build before running containers
# --detach  Run in background
docker-compose -p nsi-si-prjct-prod -f docker-compose-prod.yml up --build -d