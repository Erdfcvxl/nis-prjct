# Installation

#### Requirements:

* Install Docker https://www.docker.com/

* Install git https://git-scm.com/

#### Installation process
Run following commands: 

1. `git clone https://bitbucket.org/Erdfcvxl/nis-prjct/`

2. `cd nis-prjct`

3. `chmod +x deploy.sh`

4. `./deploy.sh`

If actions performed using root user run:
`chown -R www-data:www-data ./backend/laravel/storage`

#

Docker cleanup guide: 
https://gist.github.com/bastman/5b57ddb3c11942094f8d0a97d461b430